class MyBigNumber:
    def sum(stn1, stn2):
        result = ""
        remainder = 0
        index_stn1 = len(stn1)-1
        index_stn2 = len(stn2)-1
        while index_stn1 >=0 and index_stn2 >=0:
            temp = remainder + int(stn1[index_stn1]) + int(stn2[index_stn2])
            result = str(temp%10) +result
            remainder = temp // 10
            index_stn1-=1
            index_stn2-=1
        while index_stn1>=0:
            temp = remainder + int(stn1[index_stn1])
            result = str(temp%10) +result
            remainder = temp // 10
            index_stn1-=1
        while index_stn2>=0:
            temp = remainder + int(stn2[index_stn2])
            result = str(temp%10) +result
            remainder = temp // 10
            index_stn2-=1
        if (remainder != 0 ): result =str(remainder)+result
        return result