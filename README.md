# Add 2 numbers



## How to use  
After clone repository to your computer. Please do the following steps to compile the add2Number.py:  
1. Open the Terminal on your computer.  
2. Navigate to the directory containing my repository file using the cd command.
3. Type the following command to compile the add2Number.py file:  
```code
py add2Number.py
```  
Note: If you have both Python 2 and Python 3 installed on your computer, you may use "python3" instead of "python" to ensure that the correct version of Python is used.  
## Run Unit Testing  
Type the following command from the directory containing my repository to run unitTest file:  
```code
py .\unitTest\testSum2string.py
```  