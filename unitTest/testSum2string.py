import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
from add2Number import MyBigNumber
def test_sum():
    assert MyBigNumber.sum("123","321")== "444", "It Should be 444"
    assert MyBigNumber.sum("99999999999999","1")== "100000000000000", "It Should be 100000000000000"
    assert MyBigNumber.sum("1","99999999999999")== "100000000000000", "It Should be 100000000000000"
    assert MyBigNumber.sum("99999999999999","99999999999999")== "199999999999998", "It Should be 199999999999998"
    assert MyBigNumber.sum("99999999999999","0")== "99999999999999", "It Should be 99999999999999"
    assert MyBigNumber.sum("0","99999999999999")== "99999999999999", "It Should be 99999999999999"
    assert MyBigNumber.sum("9999","99999999999999")== "100000000009998", "It Should be 100000000009998"
    assert MyBigNumber.sum("99999999999999","9999")== "100000000009998", "It Should be 100000000009998"
if __name__=='__main__':
    test_sum()
    logging.info("Everything passed")  